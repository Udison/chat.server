package onlinechat_server;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.DefaultAccountsContainer;
import onlinechat_server.domain.containers.DefaultConnectionsContainer;
import onlinechat_server.domain.handlers.DefaultRequestHandler;
import onlinechat_server.domain.handlers.RequestHandler;
import onlinechat_server.domain.messages.AuthenticationMessage;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.domain.handlers.AuthenticationHandler;
import onlinechat_server.domain.handlers.RegistrationHandler;
import onlinechat_server.infrastructure.sender.MessageSender;
import onlinechat_server.domain.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RequestManagerTest {
    @Mock
    private Request mocRequest;
    @Mock
    private Connection mocConnection;
    @Mock
    private MessageSender sender;
    @Mock
    private RegistrationHandler spyRegistrationHandler;
    @Spy
    private AuthenticationHandler spyAuthenticationHandler;

    @InjectMocks
    RequestHandler manager = new DefaultRequestHandler();

    @Before
    public void setUp(){
        DefaultAccountsContainer.getInstance().removeAllAccounts();
        DefaultAccountsContainer.getInstance().addAccount(new User("Sergei", "A001AA", "A001AA"));
        DefaultConnectionsContainer.getInstance().removeAllConnections();
        when(mocConnection.getIp()).thenReturn("127.0.0.1");
        DefaultConnectionsContainer.getInstance().addConnection(mocConnection);
    }

    @Test (expected = NullPointerException.class)
    public void handleRequest_test_Request_is_null() {
        manager.handleRequest(mocRequest);
    }

    @Test (expected = NullPointerException.class)
    public void handleRequest_test_Request_is_empty() {
        manager.handleRequest(new Request());
    }

    @Test (expected = NullPointerException.class)
    public void handleRequest_test_Request_field_request_contains_empty_MessageWrapper() {
        manager.handleRequest(new Request(mocConnection, new MessageWrapper()));
    }

    @Test (expected = NullPointerException.class)
    public void handleRequest_test_MessageWrapper_with_EventAUTHENTICATION_and_empty_payLoad() {
        manager.handleRequest(new Request(mocConnection, new MessageWrapper(Event.AUTHENTICATION, "")));
    }

    @Test
    public void handlerRequest_should_use_readRequestMethod_which_return_correctly_AuthenticationMessage() throws IOException {
        MessageWrapper wrapperInRequest = new MessageWrapper(Event.AUTHENTICATION, "{\"login\":\"A001AA\",\"password\":\"A001AA\"}");
        AuthenticationMessage actualAuthenticationMessage = new AuthenticationMessage("A001AA", "A001AA");
        ArgumentCaptor<Request>captorToReadRequestMethod =
                ArgumentCaptor.forClass(Request.class);
        Socket mocSocket = mock(Socket.class);
        OutputStream mocOutput = mock(OutputStream.class);
        Request request = new Request(mocConnection, wrapperInRequest);

        when(mocConnection.getSocket()).thenReturn(mocSocket);
        when(mocSocket.getOutputStream()).thenReturn(mocOutput);

        manager.handleRequest(request);

        verify(spyAuthenticationHandler).readRequest(captorToReadRequestMethod.capture());
        assertThat(request, is(equalTo(captorToReadRequestMethod.getValue())));
        assertThat(actualAuthenticationMessage, is(equalTo(spyAuthenticationHandler.readRequest(request))));

    }

    @Test
    public void handlerRequest_should_use_defineUserMethod_which_use_correctly_AuthenticationMessage_and_set_user() throws IOException {
        MessageWrapper wrapperInRequest = new MessageWrapper(
                Event.AUTHENTICATION, "{\"login\":\"A001AA\",\"password\":\"A001AA\"}");
        AuthenticationMessage actualAuthenticationMessage =
                new AuthenticationMessage("A001AA", "A001AA");
        ArgumentCaptor<AuthenticationMessage>captorToDefineUserMethod =
                ArgumentCaptor.forClass(AuthenticationMessage.class);
        Socket mocSocket = mock(Socket.class);
        OutputStream mocOutput = mock(OutputStream.class);
        Request request = new Request(mocConnection, wrapperInRequest);

        when(mocConnection.getSocket()).thenReturn(mocSocket);
        when(mocSocket.getOutputStream()).thenReturn(mocOutput);

        manager.handleRequest(request);

        verify(spyAuthenticationHandler).defineUser(captorToDefineUserMethod.capture());
        assertThat(actualAuthenticationMessage, is(equalTo(captorToDefineUserMethod.getValue())));
        assertThat(new User("Sergei", "A001AA", "A001AA"),
                is(equalTo(spyAuthenticationHandler.getUser())));

    }

    @Test
    public void handleRequest_test_MessageWrapper_with_EventREGISTRATION_and_empty_payLoad() {
        ArgumentCaptor<Request> requestCaptor =
                ArgumentCaptor.forClass(Request.class);
        Request requestToHandle = new Request(mocConnection, new MessageWrapper(Event.REGISTRATION, ""));
        manager.handleRequest(requestToHandle);
        verify(spyRegistrationHandler).checkLogin(null);
        verify(spyRegistrationHandler).checkPassword(null);
        verify(spyRegistrationHandler).checkConfirmedPassword(null);
        verify(spyRegistrationHandler).checkNickName(null);
        verify(spyRegistrationHandler).notifyUser(requestCaptor.capture());
        assertThat(requestToHandle, equalTo(requestCaptor.getValue()));
        assertThat(spyRegistrationHandler.isLoginIsCorrect(), is(false));
        assertThat(spyRegistrationHandler.isPasswordIsCorrect(), is(false));
        assertThat(spyRegistrationHandler.isConfirmedPasswordIsCorrect(), is(false));
        assertThat(spyRegistrationHandler.isNicknameIsCorrect(), is(false));

    }

    @Test
    public void handleRequest_test_MessageWrapper_with_EventTOUSER_and_empty_payLoad() {
        manager.handleRequest(new Request(mocConnection, new MessageWrapper(Event.TOUSER, "")));
    }

    @Test
    public void handleRequest_test_MessageWrapper_with_EventTOALLUSERs_and_empty_payLoad() {
        Request request = new Request(mocConnection, new MessageWrapper(Event.TOALLUSERs, ""));
        ArgumentCaptor<Request>argumentCaptor =
                ArgumentCaptor.forClass(Request.class);
        manager.handleRequest(request);
        verify(sender).sendMessageToAllUsers(argumentCaptor.capture());
        assertEquals(new Request(mocConnection, new MessageWrapper(Event.TOALLUSERs, "")),
                argumentCaptor.getValue());
    }



}