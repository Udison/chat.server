package onlinechat_server.messages.sender;

import onlinechat_server.infrastructure.sender.DefaultMessageSender;
import onlinechat_server.infrastructure.sender.MessageSender;
import org.junit.Before;
import org.junit.Test;

public class DefaultMessageSenderTest {

    private MessageSender sender;

    @Before
    public void setUp() {
        sender = new DefaultMessageSender();
    }

    @Test
    public void sendResponse() {
    }

    @Test (expected = NullPointerException.class)
    public void sendMessageToUser_should_throws_NullPointerException() {
        sender.sendMessageToUser(null);
    }

    @Test
    public void sendMessageToAllUsers() {
    }
}