package onlinechat_server.messages.handlers;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.DefaultAccountsContainer;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.handlers.Authentication;
import onlinechat_server.domain.handlers.AuthenticationHandler;
import onlinechat_server.domain.messages.AuthenticationMessage;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;
import onlinechat_server.domain.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import static junit.framework.TestCase.assertNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTest {

    @Spy
    private DefaultMessageSender spySender;

    @InjectMocks
    Authentication handler = new AuthenticationHandler();

    @Before
    public void setUp() {

        DefaultAccountsContainer.getInstance().removeAllAccounts();
        DefaultAccountsContainer.getInstance().addAccount(new User("Nick_name", "Sergei", "Sss111"));

    }

    @Test
    public void readRequest_should_return_AuthenticationMessage() {
        Request mocRequest = mock(Request.class);
        MessageWrapper mocWrapper = mock(MessageWrapper.class);
        when(mocRequest.getRequest()).thenReturn(mocWrapper);
        AuthenticationMessage message = new AuthenticationMessage("Aaa", "Aaa");
        String payLoad = DefaultConverterFactory
                .getInstance()
                .getAuthenticationMessageConverter()
                .convertMessageToString(message);
        when(mocWrapper.getPayLoad()).thenReturn(payLoad);
        assertThat(message, equalTo(handler.readRequest(mocRequest)));
    }

    @Test
    public void readRequest_test_if_payLoad_is_empty_should_return_empty_AuthenticationMessage(){
        Request mocRequest = mock(Request.class);
        MessageWrapper mocWrapper = mock(MessageWrapper.class);
        when(mocRequest.getRequest()).thenReturn(mocWrapper);
        when(mocWrapper.getPayLoad()).thenReturn("");
        assertThat(new AuthenticationMessage(), equalTo(handler.readRequest(mocRequest)));
    }

    @Test
    public void readRequest_test_if_Request_is_null_should_return_empty_AuthenticationMessage() {
        assertThat(new AuthenticationMessage(), equalTo(handler.readRequest(null)));
    }

    @Test
    public void defineUser_should_set_user() {
        AuthenticationMessage mocMessage = mock(AuthenticationMessage.class);
        when(mocMessage.getLogin()).thenReturn("Sergei");
        when(mocMessage.getPassword()).thenReturn("Sss111");
        handler.defineUser(mocMessage);
        assertThat(new User("Nick_name", "Sergei", "Sss111"), equalTo(handler.getUser()));
    }

    @Test
    public void defineUser_should_stay_user_field_is_null_if_Accounts_is_not_contains_the_user() {
        AuthenticationMessage mocMessage = mock(AuthenticationMessage.class);
        when(mocMessage.getLogin()).thenReturn("Wrong login");
        handler.defineUser(mocMessage);
        assertNull(handler.getUser());

    }

    @Test
    public void defineUser_should_stay_user_field_is_null_if_password_is_different() {
        AuthenticationMessage mocMessage = mock(AuthenticationMessage.class);
        when(mocMessage.getLogin()).thenReturn("Sergei");
        when(mocMessage.getPassword()).thenReturn("Wrong password");
        handler.defineUser(mocMessage);
        assertNull(handler.getUser());
    }

    @Test
    public void notifyUser_if_user_is_null() throws IOException {

        Connection mocConnection = mock(Connection.class);
        Socket mocSocket = mock(Socket.class);
        OutputStream mocOutputStream = mock(OutputStream.class);
        ArgumentCaptor<MessageWrapper> wrapperCap =
                ArgumentCaptor.forClass(MessageWrapper.class);
        MessageWrapper actualWrapper = new MessageWrapper(Event.AUTHENTICATION_FAILED, "Wrong login or password");
        Request request = new Request(mocConnection, actualWrapper);

        when(mocConnection.getSocket()).thenReturn(mocSocket);
        when(mocSocket.getOutputStream()).thenReturn(mocOutputStream);

        handler.notifyUser(request);

        verify(spySender).sendResponse(any(Connection.class), wrapperCap.capture());
        assertThat(actualWrapper, equalTo(wrapperCap.getValue()));
    }

    @Test
    public void notifyUser_if_user_is_not_null_should_send_message_with_user() throws IOException {
        AuthenticationMessage message = new AuthenticationMessage("Sergei", "Sss111");

        Connection mocConnection = mock(Connection.class);
        Socket mocSocket = mock(Socket.class);
        OutputStream mocOutputStream = mock(OutputStream.class);
        ArgumentCaptor<MessageWrapper> wrapperCap =
                ArgumentCaptor.forClass(MessageWrapper.class);
        User actualUser = new User("Nick_name", "Sergei", "Sss111");
        try {
            String jsonByUser = DefaultConverterFactory.getInstance().getUserConverter().convertUserToString(actualUser);
            MessageWrapper actualWrapper = new MessageWrapper(Event.AUTHENTICATION_SUCCESS, jsonByUser);
            Request request = new Request(mocConnection, actualWrapper);

            when(mocConnection.getSocket()).thenReturn(mocSocket);
            when(mocSocket.getOutputStream()).thenReturn(mocOutputStream);

            handler.defineUser(message);
            handler.notifyUser(request);

            verify(spySender).sendResponse(any(Connection.class), wrapperCap.capture());
            boolean customEqual = actualWrapper.equals(wrapperCap.getValue());
            assertThat(true, equalTo(customEqual));
        } catch (ConvertException e) {
            e.printStackTrace();
        }
    }
}