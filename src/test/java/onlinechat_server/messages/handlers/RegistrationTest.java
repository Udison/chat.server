package onlinechat_server.messages.handlers;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.DefaultAccountsContainer;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.handlers.Registration;
import onlinechat_server.domain.handlers.RegistrationHandler;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.domain.messages.RegistrationMessage;
import onlinechat_server.domain.user.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RegistrationTest {

    private Registration handler;

    @Before
    public void setUp() {
        DefaultAccountsContainer.getInstance().removeAllAccounts();
        DefaultAccountsContainer.getInstance().addAccount(new User("Nick_name", "Login", "Password"));
        handler = new RegistrationHandler();
    }

    @Test (expected = NullPointerException.class)
    public void readRequest_test_request_is_null() {
        assertNull(handler.readRequest(null));
    }

    @Test (expected = NullPointerException.class)
    public void readRequest_test_empty_Request() {
        assertNull(handler.readRequest(new Request()));
    }

    @Test (expected = NullPointerException.class)
    public void readRequest_test_empty_field_request_in_Request() {
        Connection mocConnection = mock(Connection.class);
        handler.readRequest(new Request(mocConnection, new MessageWrapper()));
    }

    @Test
    public void readRequest_test_Request_is_correct() throws ConvertException {
        Request mocRequest = mock(Request.class);
        MessageWrapper mocWrapper = mock(MessageWrapper.class);
        String payLoad = DefaultConverterFactory
                .getInstance()
                .getRegistrationMessageConverter()
                .convertMessageToString(new RegistrationMessage(
                                "Nick_name",
                                "Login",
                                "Password",
                                "Password"));
        when(mocWrapper.getPayLoad()).thenReturn(payLoad);
        when(mocRequest.getRequest()).thenReturn(mocWrapper);

        assertEquals(new RegistrationMessage(
                "Nick_name",
                "Login",
                "Password",
                "Password"), handler.readRequest(mocRequest));
    }

    @Test
    public void checkLogin_test_very_long_login() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getLogin()).thenReturn("ItIsVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryVeryLongLogin");
        handler.checkLogin(mocMessage);
        assertFalse(handler.isLoginIsCorrect());
    }

    @Test (expected = NullPointerException.class)
    public void checkLogin_test_null_RegistrationMessage() {
        handler.checkLogin(null);
    }

    @Test
    public void checkLogin_test_login_with_wrong_symbols() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getLogin()).thenReturn("it is login");
        handler.checkLogin(mocMessage);
        assertFalse(handler.isLoginIsCorrect());
    }

    @Test
    public void checkLogin_test_login_is_already_in_use() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getLogin()).thenReturn("Login");
        handler.checkLogin(mocMessage);
        assertFalse(handler.isLoginIsCorrect());
    }

    @Test
    public void checkPassword_test_password_contains_wrong_symbols() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getPassword()).thenReturn("1+1");
        handler.checkPassword(mocMessage);
        assertFalse(handler.isPasswordIsCorrect());
    }

    @Test
    public void checkPassword_test_password_is_correctly() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getPassword()).thenReturn("Pp123456");
        handler.checkPassword(mocMessage);
        assertTrue(handler.isPasswordIsCorrect());
    }

    @Test
    public void checkPassword_test_password_is_very_long() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getPassword()).thenReturn("1234567891011121314151617181920");
        handler.checkPassword(mocMessage);
        assertFalse(handler.isPasswordIsCorrect());
    }

    @Test (expected = NullPointerException.class)
    public void checkPassword_test_null_RegistrationMessage() {
        handler.checkPassword(null);
    }

    @Test
    public void checkConfirmedPassword_test_confirmedPassword_is_correctly() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getPassword()).thenReturn("Sss111");
        when(mocMessage.getConfirmPassword()).thenReturn("Sss111");
        handler.checkConfirmedPassword(mocMessage);
        assertTrue(handler.isConfirmedPasswordIsCorrect());
    }
    @Test
    public void checkConfirmedPassword_test_confirmedPassword_is_different() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getPassword()).thenReturn("Sss111");
        when(mocMessage.getConfirmPassword()).thenReturn("111Sss");
        handler.checkConfirmedPassword(mocMessage);
        assertFalse(handler.isConfirmedPasswordIsCorrect());
    }

    @Test (expected = NullPointerException.class)
    public void checkConfirmedPassword_test_null_RegistrationMessage() {
        handler.checkConfirmedPassword(null);
    }

    @Test
    public void checkNickName_test_nickname_is_already_in_use() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getNickName()).thenReturn("Nick_name");
        handler.checkNickName(mocMessage);
        assertFalse(handler.isNicknameIsCorrect());
    }

    @Test
    public void checkNickName_test_nickname_is_very_long() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getNickName()).thenReturn("Nick_name_is_very_very_very_very_very_very_long");
        handler.checkNickName(mocMessage);
        assertFalse(handler.isNicknameIsCorrect());
    }

    @Test
    public void checkNickName_test_nickname_contains_wrong_symbol() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getNickName()).thenReturn("it is nick+name");
        handler.checkNickName(mocMessage);
        assertFalse(handler.isNicknameIsCorrect());
    }

    @Test
    public void checkNickName_test_nickname_is_correctly() {
        RegistrationMessage mocMessage = mock(RegistrationMessage.class);
        when(mocMessage.getNickName()).thenReturn("Free_nickname");
        handler.checkNickName(mocMessage);
        assertTrue(handler.isNicknameIsCorrect());
    }

    @Test (expected = NullPointerException.class)
    public void checkNickName_test_null_RegistrationMessage() {
        handler.checkNickName(null);
    }

        @Test
    public void notifyUser() {

    }
}