package onlinechat_server.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.RegistrationMessage;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.infrastructure.converters.RegistrationMessageConverter;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class RegistrationMessageConverterTest {

    private RegistrationMessageConverter converter;

    @Before
    public void setUp() {
        converter = DefaultConverterFactory.getInstance().getRegistrationMessageConverter();
    }

    @Test
    public void convertMessageToString_test_message_is_null() throws ConvertException {
        converter.convertMessageToString(null);

    }

    @Test
    public void convertMessageToString_test_message_is_correctly() throws ConvertException {
        RegistrationMessage actualMessage = new RegistrationMessage("A", "A", "a", "a");
        assertEquals("{\"sendTime\":null,\"ipSender\":null,\"nickName\":\"A\",\"login\":\"A\",\"password\":\"a\",\"confirmPassword\":\"a\"}",
                converter.convertMessageToString(actualMessage));
    }

    @Test (expected = ConvertException.class)
    public void convertStringToMessage_test_invalid_string() throws ConvertException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        converter.convertStringToMessage("this is invalid string");
    }

    @Test (expected = ConvertException.class)
    public void convertStringToMessage_test_should_return_null() throws ConvertException {
        converter.convertStringToMessage("");
    }

    @Test
    public void convertStringToMessage_test_string_is_correctly() throws ConvertException {
        RegistrationMessage expectedMessage = new RegistrationMessage("A", "A", "a", "a");
        assertEquals(expectedMessage, converter.convertStringToMessage("{\"sendTime\":null,\"ipSender\":null,\"nickName\":\"A\",\"login\":\"A\",\"password\":\"a\",\"confirmPassword\":\"a\"}"));
    }
}