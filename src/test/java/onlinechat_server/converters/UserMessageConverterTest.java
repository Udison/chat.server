package onlinechat_server.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.UserMessage;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.infrastructure.converters.UserMessageConverter;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class UserMessageConverterTest {

    private UserMessageConverter converter;

    @Before
    public void setUp() {
        converter = DefaultConverterFactory.getInstance().getUserMessageConverter();
    }

    @Test
    public void convertMessageToString_test_message_is_null() throws ConvertException {
        assertEquals("null", converter.convertMessageToString(null));
    }

    @Test
    public void convertMessageToString_test_message_is_correctly() throws ConvertException {
        UserMessage actualMessage = new UserMessage("Sender", "Recipient", "Text");
        assertEquals("{\"sendTime\":null,\"senderNickname\":\"Sender\",\"recipientNickname\":\"Recipient\",\"text\":\"Text\"}",
                converter.convertMessageToString(actualMessage));
    }

    @Test
    public void convertStringToMessage_test_invalid_string() throws ConvertException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        assertNull(converter.convertStringToMessage("this is invalid string"));
        assertEquals("Wrong string\r\n", outContent.toString());
    }

    @Test
    public void convertStringToMessage_test_string_is_correctly() throws ConvertException {
        UserMessage expectedMessage = new UserMessage("Sender", "Recipient", "Text");
        assertEquals(expectedMessage, converter.convertStringToMessage("{\"sendTime\":null,\"senderNickname\":\"Sender\",\"recipientNickname\":\"Recipient\",\"text\":\"Text\"}"));
    }
}