package onlinechat_server;

import lombok.Getter;
import lombok.Setter;

public class ServerContext {

    @Getter @Setter
    private boolean isRunning;
    @Getter @Setter
    private int capacity;

    private ServerContext() {
    }

    private static class ServerContextHolder {
        public static final ServerContext SERVER_CONTEXT_HOLDER = new ServerContext();
    }

    public static ServerContext getInstance() {
        return ServerContextHolder.SERVER_CONTEXT_HOLDER;
    }
}
