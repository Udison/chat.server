package onlinechat_server.dao;

import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.user.User;
import onlinechat_server.infrastructure.converters.UserConverter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DefaultUserRepository implements UserRepository {

    private String filePath;
    private UserConverter converter;

    private DefaultUserRepository() {
        this.converter = DefaultConverterFactory.getInstance().getUserConverter();

    }

    public static class DefaultUserRepositoryHolder {
        private static final DefaultUserRepository USER_REPOSITORY_HOLDER = new DefaultUserRepository();
    }

    public static DefaultUserRepository getInstance() {
        return DefaultUserRepositoryHolder.USER_REPOSITORY_HOLDER;
    }

    @Override
    public void setFilePath(String path) {
        this.filePath = path;
    }

    @Override
    public void saveUser(User user) throws IOException {
        FileWriter writer = new FileWriter(filePath, true);
        try {
            writer.write(converter.convertUserToString(user) + "\r\n");
        } catch (ConvertException e) {
            e.printStackTrace();
        }
        writer.close();
    }

    @Override
    public void saveUsers(List<User> users) throws IOException {
        for (User user : users) {
            FileWriter writer = new FileWriter(filePath);
            try {
                writer.write(converter.convertUserToString(user));
            } catch (ConvertException e) {
                e.printStackTrace();
            }
            writer.close();
        }

    }

    @Override
    public User getUserByNickname(String nickname) throws IOException {
        User result = null;
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] values = line.split(",");
            if (values[0].contains(nickname)) {
                try {
                    result = converter.convertStringToUser(line);
                } catch (ConvertException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        reader.close();
        return result;
    }


    @Override
    public User getUserByLogin(String login) throws IOException {
        User result = null;
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] values = line.split(",");
            if (values[1].contains(login)) {
                try {
                    result = converter.convertStringToUser(line);
                } catch (ConvertException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        reader.close();
        return result;
    }

    /**
     * @param date (Valid format: yyyy.MM.dd, yyyy-MM-dd yyyy/MM/dd)
     * @return User or null if user was not found with this date of registration
     * @throws IOException
     */
    @Override
    public User getUserByDateOfRegistration(String date) throws IOException {
        User result = null;
        String transformedDateToSuitableString = date.replaceAll("\\W", "-");
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(transformedDateToSuitableString)) {
                try {
                    result = converter.convertStringToUser(line);
                } catch (ConvertException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        reader.close();
        return result;
    }

    @Override
    public User getUserByDateOfRegistration(LocalDate date) throws IOException {
        User result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String suitableDate = date.format(formatter);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(suitableDate)) {
                try {
                    result = converter.convertStringToUser(line);
                } catch (ConvertException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        reader.close();
        return result;
    }

    @Override
    public List<User> getUsersByNicknames(List<String> nicknames) throws IOException {
        List<User> toReturn = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            for (String nickname : nicknames) {
                if (line.contains(nickname)) {
                    try {
                        toReturn.add(converter.convertStringToUser(line));
                    } catch (ConvertException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        reader.close();
        return toReturn;
    }

    @Override
    public List<User> getUsersByUserNames(List<String> userNames) {
        return null;
    }

    @Override
    public List<User> getUserSByDateOfRegistration(String date) throws IOException {
        List<User> toReturn = new ArrayList<>();
        String transformedDateToSuitableString = date.replaceAll("\\W", "-");
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(transformedDateToSuitableString)) {
                try {
                    toReturn.add(converter.convertStringToUser(line));
                } catch (ConvertException e) {
                    e.printStackTrace();
                }
            }
        }
        reader.close();
        return toReturn;
    }

    @Override
    public List<User> getUserSByDateOfRegistration(LocalDate date) throws IOException {
        List<User> toReturn = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String suitableDate = date.format(formatter);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.contains(suitableDate)) {
                try {
                    toReturn.add(converter.convertStringToUser(line));
                } catch (ConvertException e) {
                    e.printStackTrace();
                }
            }
        }
        reader.close();
        return toReturn;
    }

    @Override
    public List<User> getAllUsers() throws IOException {
        List<User> toReturn = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = reader.readLine()) != null) {
            try {
                toReturn.add(converter.convertStringToUser(line));
            } catch (ConvertException e) {
                e.printStackTrace();
            }
        }
        reader.close();
        return toReturn;
    }

    @Override
    public void removeUser(User user) throws IOException {
        String json = null;
        try {
            json = converter.convertUserToString(user);
        } catch (ConvertException e) {
            e.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        StringBuffer stringBuffer = new StringBuffer();
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.equals(json)) {
                stringBuffer.append(line).append("\r\n");
                break;
            }
        }
        reader.close();
    }

    @Override
    public void removeAllUsers() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        StringBuffer stringBuffer = new StringBuffer();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuffer.append(line).append("\r\n");
        }
        reader.close();

    }
}

