package onlinechat_server.dao;

import onlinechat_server.domain.user.User;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public interface UserRepository {

    void setFilePath(String path) throws IOException;
    void saveUser(User user) throws IOException;
    void saveUsers(List<User> users) throws IOException;
    User getUserByNickname(String nickname) throws IOException;
    User getUserByLogin(String login) throws IOException;
    User getUserByDateOfRegistration(String date) throws IOException;
    User getUserByDateOfRegistration(LocalDate date) throws IOException;
    List<User> getUsersByNicknames(List<String > nicknames) throws IOException;
    List<User> getUsersByUserNames(List<String> userNames) throws IOException;
    List<User> getUserSByDateOfRegistration(String date) throws IOException;
    List<User> getUserSByDateOfRegistration(LocalDate date) throws IOException;
    List<User> getAllUsers() throws IOException;
    void removeUser(User user) throws IOException;
    void removeAllUsers() throws IOException;
}
