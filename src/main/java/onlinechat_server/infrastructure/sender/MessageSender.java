/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.sender;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.messages.MessageWrapper;

/**
 *
 * @author Admin
 */
public interface MessageSender {

    void sendFromSystemToAll(MessageWrapper wrapper);
    void sendResponse(Connection connection, MessageWrapper wrapper);
    void sendMessageToUser(Request request);
    void sendMessageToAllUsers(Request request);

}
