/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.sender;


import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.DefaultConnectedUsersContainer;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.MessageToAllUsers;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.domain.messages.UserMessage;
import onlinechat_server.domain.user.User;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;

/**
 * @author Admin
 */
public class DefaultMessageSender implements MessageSender {

    public DefaultMessageSender() {
    }

//    private static class DefaultMessageSenderHolder {
//        public static final DefaultMessageSender HOLDER_INSTANCE = new DefaultMessageSender();
//    }
//
//    public static DefaultMessageSender getInstance() {
//        return DefaultMessageSenderHolder.HOLDER_INSTANCE;
//    }

    @Override
    public void sendFromSystemToAll(MessageWrapper wrapper) {
        try {
            for (Map.Entry<String, Connection> userConnections : DefaultContainerFactory
                    .getInstance()
                    .getConnections()
                    .getConnections()
                    .entrySet()) {
                Connection userConnection = userConnections.getValue();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(
                                userConnection
                                        .getSocket()
                                        .getOutputStream()));
                String jsonToSend = DefaultConverterFactory
                        .getInstance()
                        .getMessageWrapperConverter()
                        .convertWrapperToString(wrapper);
                writer.write(jsonToSend + "\r\n");
                writer.flush();
            }
        } catch (ConvertException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendResponse(Connection connection, MessageWrapper wrapper) {
        try {
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(connection
                            .getSocket()
                            .getOutputStream()));
            String jsonToSend = DefaultConverterFactory
                    .getInstance()
                    .getMessageWrapperConverter().convertWrapperToString(wrapper);
            writer.write(jsonToSend + "\r\n");
            writer.flush();
        } catch (ConvertException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessageToUser(Request request) {
        try {
            UserMessage message = DefaultConverterFactory
                    .getInstance()
                    .getUserMessageConverter()
                    .convertStringToMessage(request.getRequest().getPayLoad());
            if (checkSender(request)) {
                PrintWriter writer = new PrintWriter(
                        new OutputStreamWriter(
                                DefaultConnectedUsersContainer
                                        .getInstance()
                                        .getUserConnectionByUserNickname(message.getRecipientNickname())
                                        .getSocket()
                                        .getOutputStream()));
                String jsonToSend = DefaultConverterFactory
                        .getInstance()
                        .getMessageWrapperConverter()
                        .convertWrapperToString(request.getRequest());
                writer.write(jsonToSend + "\r\n");
                writer.flush();
            }
        } catch (ConvertException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessageToAllUsers(Request request) {
        try {
            if (checkSender(request)) {
                for (Map.Entry<User, Connection> userConnections : DefaultContainerFactory
                        .getInstance()
                        .getConnectedUsers()
                        .getUsersConnections()
                        .entrySet()) {
                    Connection userConnection = userConnections.getValue();
                    PrintWriter writer = new PrintWriter(
                            new OutputStreamWriter(
                                    userConnection
                                            .getSocket()
                                            .getOutputStream()));
                    String jsonToSend = DefaultConverterFactory
                            .getInstance()
                            .getMessageWrapperConverter()
                            .convertWrapperToString(request.getRequest());
                    writer.write(jsonToSend + "\r\n");
                    writer.flush();
                }
            }
        } catch (ConvertException | IOException e) {
            e.printStackTrace();
        }
    }


    private boolean checkSender(Request request) {
        String senderNicknameFromMessage = "";
        Connection connectionOfSenderFromMessage = null;
        try {
            switch (request.getRequest().getEvent()) {
                case TOALLUSERs:
                    MessageToAllUsers messageToAllUsers = DefaultConverterFactory
                            .getInstance()
                            .getMessageToAllUsersConverter()
                            .convertStringToMessageToAllUsers(request.getRequest().getPayLoad());
                    senderNicknameFromMessage = messageToAllUsers.getSenderNickname();
                    break;
                case TOUSER:
                    UserMessage userMessage = DefaultConverterFactory
                            .getInstance()
                            .getUserMessageConverter()
                            .convertStringToMessage(request.getRequest().getPayLoad());
                    senderNicknameFromMessage = userMessage.getSenderNickname();
                    break;
            }

            connectionOfSenderFromMessage = DefaultContainerFactory
                    .getInstance()
                    .getConnectedUsers()
                    .getUserConnectionByUserNickname(senderNicknameFromMessage);
        } catch (ConvertException e) {
            e.printStackTrace();
        }
        return connectionOfSenderFromMessage.equals(request.getConnection());
    }

}
