package onlinechat_server.infrastructure.dto;

import lombok.Getter;
import lombok.Setter;
import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.user.User;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class OnlineUsersDTO {

    @Getter @Setter
    private Set<String> onlineUsers = new HashSet<>();

    public OnlineUsersDTO() {
    }

    public OnlineUsersDTO(Set<String> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public Set<String> getOnlineUsers() {
        return onlineUsers;
    }
}
