/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.UserMessage;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 *
 * @author Admin
 */
public class UserMessageConverter {
        
    private final ObjectMapper mapper;

    private UserMessageConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class UserMessageConverterHolder {
        public static final UserMessageConverter USER_MESSAGE_CONVERTER_HOLDER =
                new UserMessageConverter();
    }

    public static UserMessageConverter getInstance() {
        return UserMessageConverterHolder.USER_MESSAGE_CONVERTER_HOLDER;
    }
    
    public String convertMessageToString(UserMessage message) throws ConvertException {
        try {
            return mapper.writeValueAsString(message);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public UserMessage convertStringToMessage(String string) throws ConvertException {
        try {
            return mapper.readValue(string, UserMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
