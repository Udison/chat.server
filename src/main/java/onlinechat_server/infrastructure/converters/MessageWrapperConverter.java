/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.MessageWrapper;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 *
 * @author Admin
 */
public class MessageWrapperConverter {
    
    private final ObjectMapper mapper;

    private MessageWrapperConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class MessageWrapperConverterHolder {
        public static final MessageWrapperConverter MESSAGE_WRAPPER_CONVERTER_HOLDER =
                new MessageWrapperConverter();
    }

    public static MessageWrapperConverter getInstance() {
        return MessageWrapperConverterHolder.MESSAGE_WRAPPER_CONVERTER_HOLDER;
    }
    
    public String convertWrapperToString(MessageWrapper wrapper) throws ConvertException {
        try {
            return mapper.writeValueAsString(wrapper);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public MessageWrapper convertStringToWrapper(String string) throws ConvertException {
        try {
            return mapper.readValue(string, MessageWrapper.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
}
