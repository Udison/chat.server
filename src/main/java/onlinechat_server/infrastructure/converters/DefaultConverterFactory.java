package onlinechat_server.infrastructure.converters;

public class DefaultConverterFactory implements ConverterFactory {

    private DefaultConverterFactory() {
    }

    private static class DefaultConverterFactoryHolder {
        public static final DefaultConverterFactory DEFAULT_CONVERTER_FACTORY_HOLDER =
                new DefaultConverterFactory();
    }

    public static DefaultConverterFactory getInstance() {
        return DefaultConverterFactoryHolder.DEFAULT_CONVERTER_FACTORY_HOLDER;
    }

    @Override
    public AuthenticationMessageConverter getAuthenticationMessageConverter() {
        return AuthenticationMessageConverter.getInstance();
    }

    @Override
    public RegistrationMessageConverter getRegistrationMessageConverter() {
        return RegistrationMessageConverter.getInstance();
    }

    @Override
    public MessageToAllUsersConverter getMessageToAllUsersConverter() {
        return MessageToAllUsersConverter.getInstance();
    }

    @Override
    public UserMessageConverter getUserMessageConverter() {
        return UserMessageConverter.getInstance();
    }

    @Override
    public MessageWrapperConverter getMessageWrapperConverter() {
        return MessageWrapperConverter.getInstance();
    }

    @Override
    public UserConverter getUserConverter() {
        return UserConverter.getInstance();
    }

    @Override
    public OnlineUsersDTOConverter getOnlineUserDTOConverted() {
        return OnlineUsersDTOConverter.getInstance();
    }
}
