/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.RegistrationMessage;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 *
 * @author Admin
 */
public class RegistrationMessageConverter {
        
    private final ObjectMapper mapper;

    private RegistrationMessageConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class RegistrationMessageConverterHolder {
        public static final RegistrationMessageConverter REGISTRATION_MESSAGE_CONVERTER_HOLDER =
                new RegistrationMessageConverter();
    }

    public static RegistrationMessageConverter getInstance() {
        return RegistrationMessageConverterHolder.REGISTRATION_MESSAGE_CONVERTER_HOLDER;
    }

    public String convertMessageToString(RegistrationMessage message) throws ConvertException {
        try {
            return mapper.writeValueAsString(message);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public RegistrationMessage convertStringToMessage(String string) throws ConvertException {
        try {
            return mapper.readValue(string, RegistrationMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
