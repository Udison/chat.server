package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.containers.ConnectedUsersContainer;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.RegistrationMessage;
import onlinechat_server.domain.user.User;
import onlinechat_server.infrastructure.dto.OnlineUsersDTO;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class OnlineUsersDTOConverter {

    private final ObjectMapper mapper;

    private OnlineUsersDTOConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class OnlineUsersDTOConverterHolder {
        public static final OnlineUsersDTOConverter ONLINE_USERS_DTO_CONVERTER_HOLDER =
                new OnlineUsersDTOConverter();
    }

    public static OnlineUsersDTOConverter getInstance() {
        return OnlineUsersDTOConverterHolder.ONLINE_USERS_DTO_CONVERTER_HOLDER;
    }

    public OnlineUsersDTO convertConnectedUsersToOnlineUsersDTO(ConnectedUsersContainer onlineUsers) {
        Set<String> toConstructorOfOnlineUsersDTO = new HashSet<>();
        for (Map.Entry<User, Connection> pair : onlineUsers.getUsersConnections().entrySet()) {
            String onlineUserNickname = pair.getKey().getNickName();
            toConstructorOfOnlineUsersDTO.add(onlineUserNickname);
        }
        return new OnlineUsersDTO(toConstructorOfOnlineUsersDTO);
    }

    public String convertOnlineUsersDTOToString(OnlineUsersDTO dto) throws ConvertException {
        try {
            return mapper.writeValueAsString(dto);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }

    public OnlineUsersDTO convertStringToOnlineUsersDTO(String string) throws ConvertException {
        try {
            return mapper.readValue(string, OnlineUsersDTO.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
}
