/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.AuthenticationMessage;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 *
 * @author Admin
 */
public class AuthenticationMessageConverter {
        
    private final ObjectMapper mapper;

    private AuthenticationMessageConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class AuthenticationMessageConverterHolder {
        public static final AuthenticationMessageConverter AUTHENTICATION_MESSAGE_CONVERTER_HOLDER =
                new AuthenticationMessageConverter();
    }

    public static AuthenticationMessageConverter getInstance() {
        return AuthenticationMessageConverterHolder.AUTHENTICATION_MESSAGE_CONVERTER_HOLDER;
    }
    
    public String convertMessageToString(AuthenticationMessage message){
        try {
            return mapper.writeValueAsString(message);
        } catch (IOException e) {
            System.out.println("Message is wrong");
            e.printStackTrace();
            return null;
        }
    }
    
    public AuthenticationMessage convertStringToMessage(String string) throws ConvertException {
        try {
            return mapper.readValue(string, AuthenticationMessage.class);
        } catch (IOException e) {
            throw new ConvertException();
        }
    }
}
