/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.MessageToAllUsers;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 *
 * @author Admin
 */
public class MessageToAllUsersConverter {
        
    private final ObjectMapper mapper;

    private MessageToAllUsersConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class MessageToAllUsersConverterHolder {
        public static final MessageToAllUsersConverter MESSAGE_TO_ALL_USERS_CONVERTER_HOLDER =
                new MessageToAllUsersConverter();
    }

    public static MessageToAllUsersConverter getInstance() {
        return MessageToAllUsersConverterHolder.MESSAGE_TO_ALL_USERS_CONVERTER_HOLDER;
    }
    
    public String convertMessageToAllUsersToString(MessageToAllUsers message) throws ConvertException {
        try {
            return mapper.writeValueAsString(message);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }
    
    public MessageToAllUsers convertStringToMessageToAllUsers(String string) throws ConvertException {
        try {
            return mapper.readValue(string, MessageToAllUsers.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }

}
