package onlinechat_server.infrastructure.converters;

import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.user.User;
import org.codehaus.jackson.map.*;

import java.io.IOException;

public class UserConverter {

    private final ObjectMapper mapper;

    private UserConverter() {
        this.mapper = new ObjectMapper();
    }

    private static class UserConverterHolder {
        public static final UserConverter USER_CONVERTER_HOLDER =
                new UserConverter();
    }

    public static UserConverter getInstance() {
        return UserConverterHolder.USER_CONVERTER_HOLDER;
    }

    public String convertUserToString(User user) throws ConvertException {
        try {
            return mapper.writeValueAsString(user);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }
    }

    public User convertStringToUser(String string) throws ConvertException {
        try {
            return mapper.readValue(string, User.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConvertException();
        }

    }
}

