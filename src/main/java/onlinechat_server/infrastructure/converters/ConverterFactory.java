package onlinechat_server.infrastructure.converters;

public interface ConverterFactory {

    AuthenticationMessageConverter getAuthenticationMessageConverter();
    RegistrationMessageConverter getRegistrationMessageConverter();
    UserMessageConverter getUserMessageConverter();
    MessageToAllUsersConverter getMessageToAllUsersConverter();
    MessageWrapperConverter getMessageWrapperConverter();
    UserConverter getUserConverter();
    OnlineUsersDTOConverter getOnlineUserDTOConverted();
}
