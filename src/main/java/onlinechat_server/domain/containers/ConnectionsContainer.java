package onlinechat_server.domain.containers;

import onlinechat_server.domain.connection.Connection;

import java.util.Map;

public interface ConnectionsContainer {

    Connection getConnection(String ip);
    Map<String, Connection> getConnections();
    void addConnection(Connection connection);
    void removeConnection(Connection connection);
    void removeAllConnections();
}
