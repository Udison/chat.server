package onlinechat_server.domain.containers;

import onlinechat_server.dao.UserRepository;

public interface ContainerFactory {

    ConnectedUsersContainer getConnectedUsers();
    ConnectionsContainer getConnections();
    AccountsContainer getAccounts();
    UserRepository getUserRepository(String repositoryName);

}
