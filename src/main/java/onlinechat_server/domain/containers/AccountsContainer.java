package onlinechat_server.domain.containers;

import onlinechat_server.domain.user.User;

import java.io.IOException;
import java.util.Map;

public interface AccountsContainer {

    void uploadAccounts() throws IOException;
    Map<String, User> getAccounts();
    void addAccount(User user);
    void removeAllAccounts();
}
