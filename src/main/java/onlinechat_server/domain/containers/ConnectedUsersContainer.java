package onlinechat_server.domain.containers;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.user.User;

import java.util.Map;

public interface ConnectedUsersContainer {

    User getUserByNickname(String nickname);
    Connection getUserConnectionByUserNickname(String nickname);
    Connection getUserConnection(User user);
    Map<User, Connection> getUsersConnections();
    void addUser(User user, Connection connection);
    void removeUserByUserNickname(String nickname);
    void removeUser(User user);
    void removeUserByConnection(Connection connection);

}
