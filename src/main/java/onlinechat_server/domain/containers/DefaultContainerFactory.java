package onlinechat_server.domain.containers;

import onlinechat_server.dao.DefaultUserRepository;
import onlinechat_server.dao.UserRepository;

public class DefaultContainerFactory implements ContainerFactory {

    private DefaultContainerFactory() {
    }

    private static class DefaultContainerFactoryHolder {
        public static final DefaultContainerFactory DEFAULT_CONTAINER_FACTORY_HOLDER = new DefaultContainerFactory();
    }

    public static DefaultContainerFactory getInstance() {
        return DefaultContainerFactoryHolder.DEFAULT_CONTAINER_FACTORY_HOLDER;
    }

    @Override
    public ConnectedUsersContainer getConnectedUsers() {
        return DefaultConnectedUsersContainer.getInstance();
    }

    @Override
    public ConnectionsContainer getConnections() {
        return DefaultConnectionsContainer.getInstance();
    }

    @Override
    public AccountsContainer getAccounts() {
        return DefaultAccountsContainer.getInstance();
    }

    @Override
    public UserRepository getUserRepository(String repositoryName) {
        return DefaultUserRepository.getInstance();
    }
}
