/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.containers;

import onlinechat_server.domain.user.User;
import onlinechat_server.dao.DefaultUserRepository;
import onlinechat_server.dao.UserRepository;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Admin
 */
public class DefaultAccountsContainer implements AccountsContainer {

    private final ConcurrentHashMap<String, User> accounts;
    private final UserRepository repository;

    private DefaultAccountsContainer() {
        this.accounts = new ConcurrentHashMap<>();
        this.repository = DefaultUserRepository.getInstance();
    }

    private static class DefaultAccountsHolder {
        public static final DefaultAccountsContainer HOLDER_INSTANCE = new DefaultAccountsContainer();
    }

    public static DefaultAccountsContainer getInstance() {
        return DefaultAccountsHolder.HOLDER_INSTANCE;
    }

    @Override
    public void uploadAccounts() throws IOException {
        List<User> users = repository.getAllUsers();
        for (User u : users) {
            accounts.put(u.getLogin(), u);
        }
    }

    @Override
    public Map<String, User> getAccounts() {
        return new ConcurrentHashMap<>(accounts);
    }

    @Override
    public void addAccount(User user) {
        accounts.put(user.getLogin(), user);
    }

    @Override
    public void removeAllAccounts() {
        accounts.clear();
    }

}
