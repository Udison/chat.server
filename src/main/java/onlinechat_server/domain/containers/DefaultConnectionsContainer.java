package onlinechat_server.domain.containers;

import onlinechat_server.domain.connection.Connection;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultConnectionsContainer implements ConnectionsContainer {

    private ConcurrentHashMap<String, Connection> connections;

    private DefaultConnectionsContainer() {
        this.connections = new ConcurrentHashMap();
    }

    private static class ConnectionsContainerHolder {
        public static final DefaultConnectionsContainer HOLDER_INSTANCE = new DefaultConnectionsContainer();
    }

    public static DefaultConnectionsContainer getInstance() {
        return ConnectionsContainerHolder.HOLDER_INSTANCE;
    }

    @Override
    public Connection getConnection(String ip) {
        return connections.get(ip);
    }

    @Override
    public Map<String, Connection> getConnections() {
        return new ConcurrentHashMap<>(connections);
    }

    @Override
    public void addConnection(Connection connection) {
        connections.put(connection.getIp(), connection);
    }

    @Override
    public void removeConnection(Connection connection) {
        connections.remove(connection.getIp());
    }

    @Override
    public void removeAllConnections() {
        connections.clear();
    }
}
