/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.containers;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.user.User;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Admin
 */
public class DefaultConnectedUsersContainer implements ConnectedUsersContainer {

    private final Map<User, Connection> onlineUsers;

    private DefaultConnectedUsersContainer() {
        this.onlineUsers = new ConcurrentHashMap<>();
    }

    private static class OnlineUsersContainerHolder {
        public static final DefaultConnectedUsersContainer HOLDER_INSTANCE = new DefaultConnectedUsersContainer();
    }

    public static DefaultConnectedUsersContainer getInstance() {
        return OnlineUsersContainerHolder.HOLDER_INSTANCE;
    }

    @Override
    public User getUserByNickname(String nickname) {
        User toReturn = null;
        for (Map.Entry<User, Connection> pair : onlineUsers.entrySet()) {
            User user = pair.getKey();
            if(user.getNickName().equals(nickname)) {
                toReturn = pair.getKey();
            }
        }
        return toReturn;
    }

    @Override
    public void addUser(User user, Connection connection) {
        onlineUsers.put(user, connection);
    }

    @Override
    public void removeUser(User user) {
        onlineUsers.remove(user);
    }

    @Override
    public Connection getUserConnectionByUserNickname(String nickname) {
        Connection userConnection = null;
        for (Map.Entry<User, Connection> pair : onlineUsers.entrySet()) {
            User user = pair.getKey();
            if(user.getNickName().equals(nickname)) {
                userConnection = pair.getValue();
            }
        }
        return userConnection;
    }

    @Override
    public Connection getUserConnection(User user) {
        return onlineUsers.get(user);
    }

    @Override
    public Map<User, Connection> getUsersConnections() {
        return new ConcurrentHashMap<>(onlineUsers);
    }

    @Override
    public void removeUserByUserNickname(String nickname) {
        User toRemove = null;
        for (Map.Entry<User, Connection> pair : onlineUsers.entrySet()) {
            User user = pair.getKey();
            if(user.getNickName().equals(nickname)) {
                toRemove = pair.getKey();
            }
        }
        onlineUsers.remove(toRemove);
    }

    @Override
    public void removeUserByConnection(Connection connection) {
        for (Map.Entry<User, Connection> pair : onlineUsers.entrySet()) {
            if(connection.equals(pair.getValue())) {
                onlineUsers.remove(pair);
            }
        }
    }
}
