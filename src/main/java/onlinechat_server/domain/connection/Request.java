package onlinechat_server.domain.connection;

import lombok.Getter;
import onlinechat_server.domain.messages.MessageWrapper;

import java.util.Objects;

@Getter
public class Request {

    private Connection connection;
    private MessageWrapper request;

    public Request() {
    }

    public Request(Connection connection, MessageWrapper request) {
        this.connection = connection;
        this.request = request;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request1 = (Request) o;
        return Objects.equals(connection, request1.connection) &&
                Objects.equals(request, request1.request);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connection, request);
    }

    @Override
    public String toString() {
        return "Request{" +
                "connection=" + connection +
                ", request=" + request +
                '}';
    }
}
