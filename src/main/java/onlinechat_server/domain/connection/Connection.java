package onlinechat_server.domain.connection;

import java.net.Socket;

public interface Connection extends Runnable {

    String getIp();
    Socket getSocket();
    void closeConnection();
    boolean isConnected();
    void setConnected(boolean connectionStatus);
    boolean isOnline();
    void setOnline(boolean onlineStatus);
}
