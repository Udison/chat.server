/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.connection;

import lombok.Getter;
import onlinechat_server.domain.containers.ConnectedUsersContainer;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.domain.handlers.DefaultRequestHandler;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.domain.readers.DefaultRequestReader;
import onlinechat_server.domain.containers.DefaultConnectionsContainer;
import onlinechat_server.domain.user.User;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;
import onlinechat_server.infrastructure.sender.MessageSender;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 *
 * @author Admin
 */
public class DefaultConnection implements Runnable, Connection {

    private final String ip;
    private final Socket socket;
    public volatile BlockingDeque<Request> requestToHandle;
    private boolean isConnected;
    private boolean isOnline;

    public DefaultConnection(Socket socket) {
        this.ip = socket.getInetAddress().getHostAddress();
        this.socket = socket;
        this.requestToHandle = new LinkedBlockingDeque<>();
        this.isConnected = true;
    }

    @Override
    public void run() {
        new Thread(new DefaultRequestReader(requestToHandle, this)).start();
        new Thread(new DefaultRequestHandler(requestToHandle, this)).start();
    }

    @Override
    public void closeConnection() {
        try {
            this.getSocket().shutdownInput();
            this.getSocket().shutdownOutput();
            this.getSocket().close();
            DefaultContainerFactory.getInstance().getConnectedUsers().removeUserByConnection(this);
            DefaultContainerFactory.getInstance().getConnections().removeConnection(this);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String getIp() {
        return this.ip;
    }

    @Override
    public synchronized  Socket getSocket() {
        return this.socket;
    }

    @Override
    public boolean isConnected() {
        return isConnected;
    }

    @Override
    public void setConnected(boolean connectionStatus) {

    }

    @Override
    public boolean isOnline() {
        return isOnline;
    }

    @Override
    public synchronized void setOnline(boolean onlineStatus) {
        isOnline = onlineStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultConnection that = (DefaultConnection) o;
        return Objects.equals(ip, that.ip) &&
                Objects.equals(socket, that.socket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, socket);
    }
}


