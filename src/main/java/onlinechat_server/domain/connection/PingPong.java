/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.connection;

import onlinechat_server.ServerContext;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;

import java.util.Map;

import static java.util.Map.Entry;

/**
 *
 * @author Admin
 */
public class PingPong implements Runnable {

    @Override
    public void run() {
        while(ServerContext.getInstance().isRunning()) {
            boolean containerIsEmpty = DefaultContainerFactory
                    .getInstance()
                    .getConnections()
                    .getConnections()
                    .isEmpty();
            if(!containerIsEmpty) {
                new DefaultMessageSender().sendFromSystemToAll(new MessageWrapper(Event.PING, ""));
                try {
                    Thread.sleep(10000);
                    checkPong();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void checkPong() {
        Map<String, Connection> connections = DefaultContainerFactory
                .getInstance()
                .getConnections()
                .getConnections();
        for (Entry<String, Connection> pair : connections.entrySet()) {
            Connection userConnection = pair.getValue();
            if (!userConnection.isOnline()) {
                //userConnection.closeConnection();
            }
            userConnection.setOnline(false);
        }
    }
}
