/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.handlers;

import onlinechat_server.domain.connection.Request;

/**
 *
 * @author Admin
 */
public interface RequestHandler {
    
    void handleRequest(Request request);
}
