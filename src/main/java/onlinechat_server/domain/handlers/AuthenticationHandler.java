/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.handlers;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.ConnectedUsersContainer;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.AuthenticationMessage;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;
import onlinechat_server.infrastructure.sender.MessageSender;
import onlinechat_server.domain.user.User;

import java.util.Map;

/**
 * @author Admin
 */
public class AuthenticationHandler implements Authentication {

    private User user;
    private MessageSender sender;

    public AuthenticationHandler() {
        this.sender = new DefaultMessageSender();
    }

    @Override
    public AuthenticationMessage readRequest(Request request) {
        if (request == null) {
            return new AuthenticationMessage();
        }
        try {
            return DefaultConverterFactory
                    .getInstance()
                    .getAuthenticationMessageConverter()
                    .convertStringToMessage(request.getRequest().getPayLoad());
        } catch (ConvertException e) {
            e.printStackTrace();
            return null;
        }

    }


    @Override
    public void defineUser(AuthenticationMessage message) {
        User u = DefaultContainerFactory.getInstance().getAccounts().getAccounts().get(message.getLogin());
        if (u == null) {
            return;
        }
        if (message.getPassword().equals(u.getPassword())) {
            user = u;
        }
    }

    @Override
    public void notifyUser(Request request) {
        if (user == null) {
            sender.sendResponse(request.getConnection(),
                    new MessageWrapper(Event.AUTHENTICATION_FAILED, "Wrong login or password"));
        } else {
            try {
                String jsonByUser = DefaultConverterFactory
                        .getInstance()
                        .getUserConverter()
                        .convertUserToString(user);
                sender.sendResponse(request.getConnection(),
                        new MessageWrapper(Event.AUTHENTICATION_SUCCESS, jsonByUser));
            } catch (ConvertException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void notifyAllUsersAboutConnection() {
        ConnectedUsersContainer onlineUsers = DefaultContainerFactory
                .getInstance()
                .getConnectedUsers();
        MessageWrapper notification = new MessageWrapper(Event.FROMSYSTEM, user.getNickName() + " is online");
        for (Map.Entry<User, Connection> pair : onlineUsers.getUsersConnections().entrySet()) {
            sender.sendResponse(pair.getValue(), notification);
        }
    }

    public User getUser() {
        return user;
    }

}
