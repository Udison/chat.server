package onlinechat_server.domain.handlers;

import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.messages.AuthenticationMessage;
import onlinechat_server.domain.user.User;


public interface Authentication {

    AuthenticationMessage readRequest(Request request);
    void defineUser(AuthenticationMessage message);
    void notifyUser(Request request);
    User getUser();
    void notifyAllUsersAboutConnection();
}
