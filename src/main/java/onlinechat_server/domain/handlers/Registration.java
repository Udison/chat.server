package onlinechat_server.domain.handlers;

import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.messages.RegistrationMessage;
import onlinechat_server.domain.user.User;

public interface Registration {

    RegistrationMessage readRequest(Request request);
    void checkLogin(RegistrationMessage message);
    void checkPassword(RegistrationMessage message);
    void checkConfirmedPassword(RegistrationMessage message);
    void checkNickName(RegistrationMessage message);
    void notifyUser(Request request);
    boolean isLoginIsCorrect();
    boolean isPasswordIsCorrect();
    boolean isConfirmedPasswordIsCorrect();
    boolean isNicknameIsCorrect();
    User getUser();
}
