/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.handlers;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.ConnectedUsersContainer;
import onlinechat_server.domain.containers.DefaultConnectionsContainer;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.domain.messages.RegistrationMessage;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.infrastructure.dto.OnlineUsersDTO;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;
import onlinechat_server.infrastructure.sender.MessageSender;

import java.util.concurrent.BlockingDeque;

/**
 * @author Admin
 */
public class DefaultRequestHandler implements Runnable, RequestHandler {

    private MessageSender sender;
    private BlockingDeque<Request> requestToHandle;
    private Connection connection;
    private Registration registrationHandler;
    private Authentication authenticationHandler;

    public DefaultRequestHandler() {
    }

    public DefaultRequestHandler(BlockingDeque<Request> requestToHandle, Connection connection) {
        this.requestToHandle = requestToHandle;
        this.sender = new DefaultMessageSender();
        this.connection = connection;
        this.registrationHandler = new RegistrationHandler();
        this.authenticationHandler = new AuthenticationHandler();
    }


    @Override
    public void run() {
        while (connection.isConnected()) {
            try {
                handleRequest(requestToHandle.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void handleRequest(Request request) {
        System.out.println(request);
        try {
            switch (request.getRequest().getEvent()) {
                case AUTHENTICATION:
                    authenticationHandler.defineUser(authenticationHandler.readRequest(request));
                    authenticationHandler.notifyUser(request);
                    if (authenticationHandler.getUser() != null) {
                        connection.setOnline(true);
                        DefaultContainerFactory
                                .getInstance()
                                .getConnectedUsers()
                                .addUser(authenticationHandler.getUser(), connection);
                        authenticationHandler.notifyAllUsersAboutConnection();
                    }
                case GIVEONLINE:
                    ConnectedUsersContainer toConverting = DefaultContainerFactory
                            .getInstance()
                            .getConnectedUsers();
                    OnlineUsersDTO dtoToSend = DefaultConverterFactory
                            .getInstance()
                            .getOnlineUserDTOConverted()
                            .convertConnectedUsersToOnlineUsersDTO(toConverting);
                    String jsonToSend = DefaultConverterFactory
                            .getInstance()
                            .getOnlineUserDTOConverted()
                            .convertOnlineUsersDTOToString(dtoToSend);
                    sender.sendResponse(request.getConnection(), new MessageWrapper(Event.GETONLINE, jsonToSend));
                    System.out.println("Send OnlineUserDTO");
                    break;
                case REGISTRATION:
                    RegistrationMessage m = registrationHandler.readRequest(request);
                    registrationHandler.checkLogin(m);
                    registrationHandler.checkPassword(m);
                    registrationHandler.checkConfirmedPassword(m);
                    registrationHandler.checkNickName(m);
                    registrationHandler.notifyUser(request);
                    break;
                case TOUSER:
                    sender.sendMessageToUser(request);
                    break;
                case TOALLUSERs:
                    sender.sendMessageToAllUsers(request);
                    break;
                case PONG:
                    DefaultConnectionsContainer.getInstance().getConnection(request
                            .getConnection()
                            .getIp()).setOnline(true);
                    break;
                case QUIT:
                    connection.setConnected(false);
                    break;
            }
        } catch (ConvertException e) {
            e.printStackTrace();
        }
    }

}
