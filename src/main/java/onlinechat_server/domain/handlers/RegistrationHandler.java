/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.handlers;


import lombok.NonNull;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.DefaultAccountsContainer;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.domain.messages.RegistrationMessage;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;
import onlinechat_server.infrastructure.sender.MessageSender;
import onlinechat_server.domain.user.User;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Admin
 */
public class RegistrationHandler implements Registration {

    private User user;
    private MessageSender sender;
    private boolean loginIsCorrect;
    private boolean passwordIsCorrect;
    private boolean confirmedPasswordIsCorrect;
    private boolean nicknameIsCorrect;

    public RegistrationHandler() {
        this.sender = new DefaultMessageSender();
    }

    @NonNull
    @Override
    public RegistrationMessage readRequest(Request request) {
        try {
            return DefaultConverterFactory
                    .getInstance()
                    .getRegistrationMessageConverter()
                    .convertStringToMessage(request.getRequest().getPayLoad());
        } catch (ConvertException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void checkLogin(RegistrationMessage message) {
        if (DefaultAccountsContainer.getInstance().getAccounts().containsKey(message.getLogin())) {
            return;
        }
        Pattern pattern = Pattern.compile("\\W");
        Matcher matcher = pattern.matcher(message.getLogin());
        boolean containsWrongSymbols = matcher.find();
        if (containsWrongSymbols || message.getLogin().length() > 16) {
            return;
        }
        loginIsCorrect = true;
    }

    @Override
    public void checkPassword(RegistrationMessage message) {
        Pattern pattern = Pattern.compile("\\W");
        Matcher matcher = pattern.matcher(message.getPassword());
        boolean containsWrongSymbols = matcher.find();
        if (containsWrongSymbols || message.getPassword().length() > 25) {
            return;
        }
        passwordIsCorrect = true;
    }

    @Override
    public void checkConfirmedPassword(RegistrationMessage message) {
        if (message.getPassword().equals(message.getConfirmPassword())) {
            confirmedPasswordIsCorrect = true;
        }
    }

    @Override
    public void checkNickName(RegistrationMessage message) {
        Pattern pattern = Pattern.compile("\\W");
        Matcher matcher = pattern.matcher(message.getNickName());
        boolean containsWrongSymbols = matcher.find();
        if (containsWrongSymbols || message.getNickName().length() > 25) {
            return;
        }
        for (Map.Entry<String, User> accounts : DefaultAccountsContainer.getInstance().getAccounts().entrySet()) {
            User user = accounts.getValue();
            if (message.getNickName().equals(user.getNickName())) {
                return;
            }
        }
        nicknameIsCorrect = true;
    }

    @Override
    public void notifyUser(Request request) {
        if (loginIsCorrect && passwordIsCorrect && confirmedPasswordIsCorrect && nicknameIsCorrect) {
            try {
                RegistrationMessage message = DefaultConverterFactory
                        .getInstance()
                        .getRegistrationMessageConverter()
                        .convertStringToMessage(request.getRequest().getPayLoad());
                user = new User(message.getNickName(), message.getLogin(), message.getPassword());
                String jsonByUser = DefaultConverterFactory
                        .getInstance()
                        .getUserConverter()
                        .convertUserToString(user);
                sender.sendResponse(request.getConnection(),
                        new MessageWrapper(Event.REGISTRATION_SUCCESS, jsonByUser));
            } catch (ConvertException e) {
                e.printStackTrace();
            }
        }

        if (!loginIsCorrect) {
            sender.sendResponse(request.getConnection(),
                    new MessageWrapper(Event.REGISTRATION_FAILED, "Login is wrong"));
        }

        if (!passwordIsCorrect) {
            sender.sendResponse(request.getConnection(),
                    new MessageWrapper(Event.REGISTRATION_FAILED, "Password is wrong"));
        }

        if (!confirmedPasswordIsCorrect) {
            sender.sendResponse(request.getConnection(),
                    new MessageWrapper(Event.REGISTRATION_FAILED, "Confirmed password is wrong"));
        }

        if (!nicknameIsCorrect) {
            sender.sendResponse(request.getConnection(),
                    new MessageWrapper(Event.REGISTRATION_FAILED, "Nickname is wrong or already used"));
        }
    }

    @Override
    public boolean isLoginIsCorrect() {
        return loginIsCorrect;
    }

    @Override
    public boolean isPasswordIsCorrect() {
        return passwordIsCorrect;
    }

    @Override
    public boolean isConfirmedPasswordIsCorrect() {
        return confirmedPasswordIsCorrect;
    }

    @Override
    public boolean isNicknameIsCorrect() {
        return nicknameIsCorrect;
    }

    @Override
    public User getUser() {
        return user;
    }
}
