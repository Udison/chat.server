package onlinechat_server.domain.readers;

public interface RequestReader extends Runnable {

    String readRequest();
    void addToQueue(String string);
}
