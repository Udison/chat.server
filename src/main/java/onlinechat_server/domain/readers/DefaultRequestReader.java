/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.readers;


import lombok.Getter;
import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.Request;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.infrastructure.converters.DefaultConverterFactory;
import onlinechat_server.domain.exceptions.ConvertException;
import onlinechat_server.domain.messages.Event;
import onlinechat_server.domain.messages.MessageWrapper;
import onlinechat_server.infrastructure.sender.DefaultMessageSender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingDeque;

/**
 * @author Admin
 */
@Getter
public class DefaultRequestReader implements Runnable, RequestReader {

    private final BlockingDeque<Request> requestToHandle;
    private final Connection connection;
    private BufferedReader reader;

    public DefaultRequestReader(BlockingDeque<Request> requestToHandle, Connection connection) {
        this.requestToHandle = requestToHandle;
        this.connection = connection;
    }

    @Override
    public void run() {
        try {
            reader = new BufferedReader(new InputStreamReader(connection
                    .getSocket()
                    .getInputStream()));
            while (connection.isConnected()) {
                String request = reader.readLine();
                if (checkRequest(request)) {
                    addToQueue(request);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            DefaultContainerFactory
                    .getInstance()
                    .getConnectedUsers()
                    .removeUserByConnection(connection);
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public String readRequest() {
        try {
            String request = reader.readLine();
            System.out.println(request);
            return request;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addToQueue(String string) {
        try {
            MessageWrapper wrapper = DefaultConverterFactory
                    .getInstance()
                    .getMessageWrapperConverter()
                    .convertStringToWrapper(string);
            requestToHandle.add(new Request(connection, wrapper));
        } catch (ConvertException e) {
            new DefaultMessageSender()
                    .sendResponse(connection, new MessageWrapper(
                            Event.EXCEPTION, "Wrong request. Try again or contact support"));
            e.printStackTrace();
        }
    }

    private boolean checkRequest(String string) {
        boolean result = false;
        if (string != null && !string.equals("")) {
            try {
                MessageWrapper wrapper = DefaultConverterFactory
                        .getInstance()
                        .getMessageWrapperConverter()
                        .convertStringToWrapper(string);
                result = wrapper.getPayLoad() != null;
                return result;
            } catch (ConvertException e) {
                new DefaultMessageSender()
                        .sendResponse(connection, new MessageWrapper(
                                Event.EXCEPTION, "Wrong request. Try again or contact support"));
                e.printStackTrace();
            }
        }
        return result;
    }

}
