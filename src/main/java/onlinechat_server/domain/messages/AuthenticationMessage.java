/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.messages;

import java.util.Objects;

/**
 *
 * @author Admin
 */
public class AuthenticationMessage {
        
    private String login;
    private String password;

    public AuthenticationMessage() {
    }

    public AuthenticationMessage(String login, String password) {
        this.login = login;
        this.password = password;
    }


    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthenticationMessage message = (AuthenticationMessage) o;
        return Objects.equals(login, message.login) &&
                Objects.equals(password, message.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password);
    }
}
