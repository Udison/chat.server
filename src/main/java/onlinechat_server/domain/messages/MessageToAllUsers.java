/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server.domain.messages;


/**
 *
 * @author Admin
 */
public class MessageToAllUsers extends Message {
    
    private String senderIp;
    private String senderNickname;
    private String text;

    public MessageToAllUsers() {
    }

    public MessageToAllUsers(String senderNickname, String text) {
        this.senderNickname = senderNickname;
        this.text = text;
    }

    public String getSenderIp() {
        return senderIp;
    }

    public String getSenderNickname() {
        return senderNickname;
    }

    public String getText() {
        return text;
    }

    public void setSenderIp(String senderIp) {
        this.senderIp = senderIp;
    }
    
}
