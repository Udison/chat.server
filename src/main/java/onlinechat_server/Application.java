/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package onlinechat_server;

import onlinechat_server.domain.connection.Connection;
import onlinechat_server.domain.connection.DefaultConnection;
import onlinechat_server.domain.connection.PingPong;
import onlinechat_server.domain.containers.DefaultConnectionsContainer;
import onlinechat_server.domain.containers.DefaultContainerFactory;
import onlinechat_server.domain.exceptions.ConvertException;

import java.io.IOException;
import java.net.ServerSocket;

/**
 *
 * @author Admin
 */
public class Application {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8080);
        System.out.println("Server started...");
        ServerContext.getInstance().setRunning(true);
        DefaultContainerFactory
                .getInstance()
                .getUserRepository("file")
                .setFilePath("C:\\Users\\Admin\\Documents\\Users.txt");
        DefaultContainerFactory
                .getInstance()
                .getAccounts()
                .uploadAccounts();
        System.out.println("Accounts uploaded...");
        new Thread(new PingPong()).start();
        System.out.println("PingPong started...");

        while(true) {
            Connection connection = new DefaultConnection(server.accept());
            new Thread(connection).start();
            DefaultConnectionsContainer.getInstance().addConnection(connection);
            System.out.println("Connection accepted...");
        }

    }
}
